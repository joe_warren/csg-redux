{-# LANGUAGE DeriveFunctor #-}
module Csg 
( AtLeast3 (..)
, Poly (..)
) where

import qualified Linear.V3 as V3
import Linear.V3 (cross)
import Linear.Vector ((^-^)) 
import Linear.Metric (normalize) 

data AtLeast3 a = AtLeast3 a a a [a] deriving (Show, Functor)

al3List :: AtLeast3 a -> [a]
al3List (AtLeast3 x y z w) = x:y:z:w

type Poly = AtLeast3 (V3.V3 Float)

data Plane = Plane
    { origin :: V3.V3 Float
    , normal ::  V3.V3 Float
    }

data Partition a = Partition 
    { front     :: [a] 
    , coplanar  :: [a] 
    , behind    :: [a]
    } deriving (Show, Functor)

toPlane :: Poly -> Plane
toPlane (AtLeast3 a b c _) = let normal = (normalize ((b ^-^ a) `cross` (c ^-^ a))) in
    Plane a normal
